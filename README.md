# wiki.ageei.org-pages

Le [wiki de l'AGEEI][1] permet à ses membres de partager des ressources
ou de l'information qui peuvent leur être utile dans leur parcours
académique, professionel et personnel.


## Comment contribuer

Pour éviter les abus, les contributions au wiki se font par le biais
de "demandes de changements" via le [dépôt git officiel du wiki][2].
Si vous n'êtes pas familier avec git et souhaitez quand même faire une
contribution, nous vous invitons à créer un [ticket][3] décrivant le
changement que vous souhaitez effectuer.

#### Exemple

Créer une fourche du projet.

**TODO insérer capture d'écran**

```
git clone https://gitlab.com/mon-utilisateur/wiki.ageei.org
cd wiki.ageei.org
editor cours/INF0123.page # faites vos changements
git add cours/INF0123.page
git commit # décrivez le changement et pourquoi
git push
```

Faites votre MR.

**TODO insérer capture d'écran**


[1]: https://wiki.ageei.org/
[2]: https://gitlab.com/ageei/wiki.ageei.org/
[3]: https://gitlab.com/ageei/wiki.ageei.org/issues
